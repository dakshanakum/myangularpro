import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatMenuModule} from '@angular/material';
import {MatGridListModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatRadioModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatDatepickerModule , MatNativeDateModule} from '@angular/material'; 
import {MatIconModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material';
import {MatTableModule} from '@angular/material';

import {HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule, ActivatedRoute } from '@angular/router';

import { Mypipe } from './component/custpipe.pipe';

import { AppComponent } from './component/app-component/app.component';
import { Topbar } from './component/topbar/topbar.component';
import { Footer } from './component/footer/footer.component';
import { Copyright } from './component/copyright/copyright.component';
import { Loginuser } from './component/login/login.component';
import { Registers } from './component/register/register.component';
import { Viewdetail } from './component/viewdetail/viewdetail.component';
import { Exercise } from './component/exercise/exercise.component';
import { Product } from './component/product/product.component';
import {Productdetail} from './component/productdetail/productdetail.component';

import { exer } from './utilities/exercise.utilities';

import {Mydirective} from './directive/stylehigh.directive';
import { Menus } from './directive/menu.directive';


const root : Routes=[
  {path:'login', component:Loginuser},
  {path:'register', component:Registers},
  {path:'product',component:Product},
  {path:'product/:id',component:Productdetail}
]

@NgModule({
  declarations: [
    AppComponent,
    Topbar,
    Footer,
    Copyright,
    Loginuser,
    Registers,
    Viewdetail,
    Exercise,
    Product,
    Productdetail,
    Mydirective,
    Menus,
    Mypipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatGridListModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatExpansionModule,
    MatTableModule,
    HttpClientModule,
    RouterModule.forRoot(root)
  ],
  providers: [exer],
  bootstrap: [AppComponent]
})
export class AppModule { }

