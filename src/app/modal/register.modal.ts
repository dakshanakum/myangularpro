export class Register{
    name : string;
    email : string;
    contact : string;
    gender : string;
    dob : string;
    country : string;
    city: string;
    address : string;
    pincode : string;
    language : string;
    description : string;
    password : string;
}