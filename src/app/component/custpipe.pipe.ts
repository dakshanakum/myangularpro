import { Pipe , PipeTransform } from '@angular/core';

@Pipe({
    name:'mypipe'
})

export class Mypipe implements PipeTransform{
    transform(value:string):number{
        return value.length;
    }
}