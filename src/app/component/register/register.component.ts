import { Component } from '@angular/core';
import { Register } from '../../modal/register.modal';
import {HttpClient} from '@angular/common/http';

@Component({
    selector : 'register',
    templateUrl : './register.component.html',
    styleUrls : ['./register.component.css']
})

export class Registers{
    title="register";

    country = [
        {value: '1', viewValue: 'India'},
        {value: '2', viewValue: 'US'},
        {value: '3', viewValue: 'Canada'}
      ];
    result : any;

    constructor(private httpc : HttpClient){}

    reg: Register = new Register();
    funreg(){
        this.result=JSON.stringify(this.reg);

        this.httpc.post('http://localhost/3000/register',this.reg).subscribe(res => {
            console.log(res);
        })
    }

}