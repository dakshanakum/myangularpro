import { Component , Input } from '@angular/core';
import { inquiry } from '../../modal/inquiry.modal';

@Component({
    selector:'footer',
    templateUrl:'./footer.component.html',
    styleUrls:['./footer.component.css']
})

export class Footer{    
    title="footer";

    @Input () a : any;

    inq: inquiry = new inquiry();
    funsubmit(){

    console.log( this.inq.name + this.inq.email + this.inq.contact + this.inq.message );
    }
}