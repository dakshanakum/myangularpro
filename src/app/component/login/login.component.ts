import { Component } from '@angular/core';
import { Login } from '../../modal/login.modal';
import {HttpClient} from '@angular/common/http';

@Component({
    selector:'login',
    templateUrl:'./login.component.html',
    styleUrls:['./login.component.css']
})

export class Loginuser{
    title="Login";
    result : any;

    constructor(private httpcl : HttpClient){        
    }
    lo : Login =new Login();
    funlogin(){
        //this.httpcl.get('http://localhost:3000/login');
        this.result=JSON.stringify(this.lo);

        this.httpcl.post('localhost:3000/login',this.lo).subscribe(res => {
            console.log(res);
        });
        console.log(this.lo.username + this.lo.password);
    }

}