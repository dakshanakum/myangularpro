import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { productlist } from '../../modal/product.modal';
import { Router } from '@angular/router';
import {trigger, transition,style, animate} from '@angular/animations';

@Component({
    selector:'product',
    templateUrl:'./product.component.html',
    styleUrls:['./product.component.css'],
    animations: [
        trigger('st', [
            transition('* =>void',[
                animate(100, style({transform:'translateY(25%)'}))
            ])
        ])
    ]
})

export class Product{
    title="Product";
    titleheading = false;

    productli:productlist[]= [];

    constructor(private httpc:HttpClient, private Rout : Router){}
    ngOnInit(){
        this.httpc.get<productlist[]>('/assets/json/productList.json').subscribe(res=>{
           this.productli=res;
           console.log(this.productli);
        })
    }

    funprodetail(ids:number){
        this.Rout.navigate(['/product',ids])
    }

    funntoggle(){
        this.titleheading = !this.titleheading;
    }

}