import { Component } from '@angular/core';

@Component({
    selector : 'viewdetail',
    templateUrl : './viewdetail.component.html',
    styleUrls : ['./viewdetail.component.css']
})

export class Viewdetail{
    title="View Detail";

    displayedColumns = ['userId', 'userName'];
}