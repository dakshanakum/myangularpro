import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { productlist } from '../../modal/product.modal';
import { ActivatedRoute } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';
import {trigger, transition,style, animate} from '@angular/animations';
@Component({
    selector:'productdetail',
    templateUrl:'./productdetail.component.html',
    styleUrls:['./productdetail.component.css'],
    animations: [
        trigger('st', [
            transition('* =>void',[
                animate(100, style({transform:'translateX(-25%)'}))
            ])
        ])
    ]
})

export class Productdetail{
    title="productdetail";
bolvar = true;
    idval:number;
    prodetail=[];

    dob = new Date(1991,6,6);

    Prolist : productlist[]=[];

    selprolist : productlist = new productlist();

    constructor( private httpc : HttpClient, private acroute : ActivatedRoute){
        this.acroute.params.subscribe(res=>{
        this.idval=res['id'];
        console.log(this.idval);
        })
    }

    toggleBool(){
        this.bolvar = !this.bolvar;
    }
    
    ngOnInit(){
        this.httpc.get<productlist[]>('/assets/json/productList.json').subscribe(res=>{
            this.Prolist=res;
            this.Prolist.forEach(respro =>{
                if(respro.id==this.idval){
                    this.selprolist=respro;
                }
            })

        })
    }

    
}