import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { productlist } from '../../modal/product.modal';

@Component({
    selector:'topbar',
    templateUrl:'./topbar.component.html',
    styleUrls:['./topbar.component.css']
})

export class Topbar{

    title="This is a topbar";

    Prolist : productlist[]=[];

    constructor(private Rout : Router, private httpc : HttpClient){}

    ngOnInit(){
        this.httpc.get<productlist[]>('/assets/json/productList.json').subscribe(res=>{
            this.Prolist=res;
            console.log(this.Prolist);
        })
    }

    funnavigate(name:string){
        switch(name){
            case 'login':
                this.Rout.navigate(['/login']);
                break;

                case 'register':
                this.Rout.navigate(['/register']);
                break;
        }
    }
    funnproduct(){
        this.Rout.navigate(['/product']);
    }
    funprodetail(ids:number){
        this.Rout.navigate(['/product',ids])
    }
}