import { Component } from '@angular/core';
import { Exercises } from '../../modal/exercise.modal';
import { exer } from '../../utilities/exercise.utilities';

@Component({
    selector:'exercise',
    templateUrl:'./exercise.component.html',
    styleUrls:['./exercise.component.css']
})

export class Exercise{
    title="exercise";

    result :number;

    constructor(private exes:exer){}

    Exe : Exercises = new Exercises();

    funexe(){
       // this.result=JSON.stringify(this.Exe);
        //console.log(this.result);

        this.result=this.exes.addition(this.Exe.first,this.Exe.second);
        console.log(this.result);
        this.result=this.exes.sub(this.Exe.first,this.Exe.second);
        console.log(this.result);
        this.result=this.exes.mul(this.Exe.first,this.Exe.second);
        console.log(this.result);
        this.result=this.exes.divi(this.Exe.first,this.Exe.second);
        console.log(this.result);
    }
}