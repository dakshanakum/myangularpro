import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
    selector:'[menus]'
})

export class Menus{
    @Input() menucolor :string;

    constructor(private elemenu: ElementRef){
       /* switch (this.menucolor) {
            case 'menubg':
                this.elemenu.nativeElement.style.backgroundColor='#ececec';
                this.elemenu.nativeElement.style.padding='5px';
                break;
        
            default:
                break;
        } */
    }
    
    @HostListener('mouseover') onMouseOver(){
        this.elemenu.nativeElement.style.backgroundColor="orange";
        this.elemenu.nativeElement.style.color="#fff";
    }
    @HostListener('mouseleave') onMouseLeave(){
        this.elemenu.nativeElement.style.backgroundColor="#fff";
        this.elemenu.nativeElement.style.color="#000";
    }
}