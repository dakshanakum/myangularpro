import { Directive, ElementRef } from '@angular/core';

@Directive({
    selector:'[mystyle]'
})

export class Mydirective{

    constructor(private eleref : ElementRef){
        this.eleref.nativeElement.style.backgroundColor='red';
        this.eleref.nativeElement.style.textAlign='left';
        this.eleref.nativeElement.style.padding='5px';
        this.eleref.nativeElement.style.color='#fff';
    }

}